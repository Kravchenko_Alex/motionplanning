package motionPlanning;

import java.awt.*;

public class RRT extends Thread{
    Field2D field2D;
    int goal_ratio;

    RRT (Field2D f, int goal_ratio) {
        super();
        field2D = f;
        this.goal_ratio = goal_ratio;
    }

    public void run() {
        long startTime = System.currentTimeMillis();
        for (int i = field2D.max_node_num; i > 0; ) {
            if (field2D.isGoalAchieved()) {
                field2D.show_path.set(true);
                break;
            }
            Point q_rand;
            if (goal_ratio > 0) {
                int choice = (int) (Math.random() * (101));
                if (choice <= goal_ratio) {
                    Point last_add_to_tree = field2D.roadmap.getCoordPoint(field2D.roadmap.size() - 1);
                    q_rand = new Point((field2D.width - 2), last_add_to_tree.y);
                }
                else
                    q_rand = field2D.randPoint();
            }
            else
                q_rand = field2D.randPoint();
            int nearest = field2D.roadmap.nearestTo(q_rand);
            Point q_near = field2D.roadmap.getCoordPoint(nearest);
            Point q_s = field2D.localPlanner(nearest, q_rand);
            if (!q_s.equals(q_near)) {
                field2D.roadmap.addVertexAndEdge(nearest, q_s);
                i--;
            }
        }
        long timeSpent = System.currentTimeMillis() - startTime;
        System.out.println("Searching time " + timeSpent + " millisecond");
        System.out.println("Nodes " + field2D.roadmap.size());
        field2D.repaint();
    }
}
