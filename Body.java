package motionPlanning;

import javax.swing.*;
import java.awt.Graphics;
import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;

abstract class Body extends JComponent implements Comparable<Body> {

    protected Point top_left;
    protected Point bottom_right;
    protected Color color;
    protected String className;

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        paintBody(g);
    }

    public double distanceBetween(Point v, Point u) {
        double dx = v.x - u.x;
        double dy = v.y - u.y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    @Override
    public int compareTo(Body b) {
        if (this.top_left.x < b.top_left.x)
            return -1;
        else if (this.top_left.x > b.top_left.x)
            return +1;
        else
            return 0;
    }


    public void setColor(Color c) { color = c;}

    abstract public boolean collideWith(Body body);

    abstract protected void paintBody(Graphics g);

}


class Rectangle extends Body {

    protected int width;
    protected int height;

    Rectangle(Point top_left, int width, int height) {
        this.top_left = top_left;
        this.width = width;
        this.height = height;
        bottom_right = new Point(top_left.x + width, top_left.y + height);
        setSize(width, height);
        setLocation(top_left);
        color = Color.BLACK;
        className = "Rectangle";
//        setOpaque(true);
//        setBackground(new Color(0,0,0));
    }

    @Override
    public boolean collideWith(Body body) {
        if (body.className.equals("Rectangle"))
            return collideWith((Rectangle) body);
        else if (body.className.equals("Circle"))
            return collideWith((Circle) body);
        else {
            System.out.println("This class does not support collision check with class " + body.className);
            return false;
        }
    }

    protected boolean collideWith(Rectangle b) {
        if (top_left.x < b.bottom_right.x && bottom_right.x > b.top_left.x &&
                 b.bottom_right.y > top_left.y  && bottom_right.y > b.top_left.y)
            return true;
        return false;
    }

    protected boolean collideWith(Circle circle) {
        if (top_left.x < circle.bottom_right.x && bottom_right.x > circle.top_left.x &&
                circle.bottom_right.y > top_left.y && bottom_right.y > circle.top_left.y)
        {
            if (circle.center.x < top_left.x && circle.center.y < top_left.y) {
                if(circle.radius > distanceBetween(circle.center, top_left))
                    return true;
                return false;
            }
            else if (circle.center.x < top_left.x && circle.center.y > bottom_right.y) {
                if(circle.radius > distanceBetween(circle.center, new Point(top_left.x, bottom_right.y)))
                    return true;
                return false;
            }
            else if (circle.center.x > bottom_right.x && circle.center.y < top_left.y) {
                if(circle.radius > distanceBetween(circle.center, new Point(bottom_right.x, top_left.y)))
                    return true;
                return false;
            }
            else if (circle.center.x > bottom_right.x && circle.center.y > bottom_right.y) {
                if(circle.radius > distanceBetween(circle.center, bottom_right))
                    return true;
                return false;
            }
            else
                return true;
        }
        return false;
    }

    public Point getLocation(){ return top_left; }

    @Override
    protected void paintBody(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        g2.setStroke(new BasicStroke(2));
        g2.setPaint(color);
        g2.draw(new Rectangle2D.Float(1, 1, width - 3, height - 3));

    }
}


class Circle extends Body {

    protected int radius;
    protected Point center;


    Circle(Point center, int radius) {
        this.center = center;
        this.radius = radius;
        top_left = new Point(center.x - radius, center.y - radius);
        bottom_right = new Point(center.x + radius, center.y + radius);
        setSize(2 * radius, 2 * radius);
        setLocation(top_left);
        color = Color.BLACK;
        className = "Circle";
    }

    @Override
    public boolean collideWith(Body body) {
        if (body.className.equals("Rectangle"))
            return collideWith((Rectangle) body);
        else if (body.className.equals("Circle"))
            return collideWith((Circle) body);
        else {
            System.out.println("This class does not support collision check with class " + body.className);
            return false;
        }
    }

    protected boolean collideWith(Circle a) {
        if ((radius + a.radius) > distanceBetween(center, a.center))
            return true;
        return false;
    }

    protected boolean collideWith(Rectangle rectangle) {
        if (rectangle.top_left.x < bottom_right.x && rectangle.bottom_right.x > top_left.x &&
                bottom_right.y > rectangle.top_left.y && rectangle.bottom_right.y > top_left.y)
        {
            if (center.x < rectangle.top_left.x && center.y < rectangle.top_left.y) {
                if(radius > distanceBetween(center, rectangle.top_left))
                    return true;
                return false;
            }
            else if (center.x < rectangle.top_left.x && center.y > rectangle.bottom_right.y) {
                if(radius > distanceBetween(center, new Point(rectangle.top_left.x, rectangle.bottom_right.y)))
                    return true;
                return false;
            }
            else if (center.x > rectangle.bottom_right.x && center.y < rectangle.top_left.y) {
                if(radius > distanceBetween(center, new Point(rectangle.bottom_right.x, rectangle.top_left.y)))
                    return true;
                return false;
            }
            else if (center.x > rectangle.bottom_right.x && center.y > rectangle.bottom_right.y) {
                if(radius > distanceBetween(center, rectangle.bottom_right))
                    return true;
                return false;
            }
            else
                return true;
        }
        return false;
    }

    public Point getLocation() {
        return top_left; }

    public void changeLocation(Point center) {
        //calculate top_left point of robot from new center point
        this.center = center;
        top_left = new Point(center.x - radius, center.y - radius);
        bottom_right = new Point(center.x + radius, center.y + radius);
        setLocation(top_left.x, top_left.y);
    }

    @Override
    protected void paintBody(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        g2.setStroke(new BasicStroke(2));
        g2.setPaint(color);
        g2.draw(new Ellipse2D.Float(1, 1, 2 * radius - 3, 2 * radius - 3));
        g2.fill(new Ellipse2D.Float( radius - 1,  radius - 1 , 2, 2 ));
    }
}