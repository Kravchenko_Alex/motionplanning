package motionPlanning;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

public class WindowGUI {
    Field2D field;


    WindowGUI() {
        JFrame jfrm = new JFrame("RRT");
        jfrm.setLayout(null);
        jfrm.setResizable(false);
        jfrm.setSize(1000, 600);
        jfrm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        field = new Field2D();

        JButton b_start = new JButton("Start");
        b_start.setSize(120,30);
        b_start.setLocation(860, 25);
        b_start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new RRT(field, 10).start();
            }
        });

        JButton b_show_tree = new JButton("Show Tree");
        b_show_tree.setSize(120,30);
        b_show_tree.setLocation(860, 70);
        b_show_tree.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                field.show_tree.set(true);
                field.repaint();
            }
        });

        JButton b_restart = new JButton("Restart");
        b_restart.setSize(120,30);
        b_restart.setLocation(860, 115);
        b_restart.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                field.roadmap = new Roadmap(field.max_node_num, field.ptStart.x, field.ptStart.y);
                field.show_tree = new AtomicBoolean(false);
                field.show_path = new AtomicBoolean(false);
                field.repaint();
            }
        });

/*        RRT rrt = new RRT(field, 10);
        rrt.run();*/

        jfrm.add(field);
        jfrm.add(b_start);
        jfrm.add(b_show_tree);
        jfrm.add(b_restart);

        jfrm.setVisible(true);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new WindowGUI();
            }
        });

        System.out.println("main is over");
    }
}
