package motionPlanning;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicBoolean;

class Field2D extends JPanel {
    AtomicBoolean show_tree;
    AtomicBoolean show_path;
    int num_steps;
    int height;
    int width;
    int max_node_num;
    ArrayList<Body> obstacles;
    Roadmap roadmap;
    Circle robot;
    Point ptStart = new Point(new Point(50, 275));

    Field2D() {
        height = 550;
        width = 840;
        num_steps = 6;
        max_node_num = 250;
        show_tree = new AtomicBoolean(false);
        show_path = new AtomicBoolean(false);
        obstacles = new ArrayList<>();
        setLayout(null);
        setSize(width, height);
        setBorder(BorderFactory.createLineBorder(Color.BLACK, 2));
        setLocation(10,10);
        setBackground(new Color(255,255,255));

        robot = new Circle(ptStart, 40);
        // add robot copy to the field for visualisation
        Circle visible_robot = new Circle(new Point(ptStart.x, ptStart.y),  robot.radius);
        visible_robot.setColor(Color.BLUE);
        add(visible_robot);

//        fieldConfig1();
//        fieldConfig2();
//        fieldConfig3();
//        fieldConfig4();
        fieldConfig5();

        Collections.sort(obstacles);

        roadmap = new Roadmap(max_node_num, robot.center.x, robot.center.y);
    }

    private void fieldConfig1() {
        obstacles.add(new Rectangle(new Point(370,40), 200 , 270));
        obstacles.add(new Rectangle(new Point(370,405), 180, 110));
        add(obstacles.get(0));
        add(obstacles.get(1));
    }

    private void fieldConfig2() {
        obstacles.add(new Circle(new Point(505,175), 135));
        obstacles.add(new Circle(new Point(505,460), 55));
        add(obstacles.get(0));
        add(obstacles.get(1));
    }

    private void fieldConfig3() {
        obstacles.add(new Rectangle(new Point(370,20), 200 , 100));
        obstacles.add(new Rectangle(new Point(370,215), 200 , 120));
        obstacles.add(new Rectangle(new Point(370,430), 200, 100));
        add(obstacles.get(0));
        add(obstacles.get(1));
        add(obstacles.get(2));
    }

    private void fieldConfig4() {
        obstacles.add(new Rectangle(new Point(300,40), 120 , 100));
        obstacles.add(new Rectangle(new Point(300,285), 120 , 100));
        obstacles.add(new Rectangle(new Point(515,145), 120, 100));
        obstacles.add(new Rectangle(new Point(515,390), 120 , 100));
        add(obstacles.get(0));
        add(obstacles.get(1));
        add(obstacles.get(2));
        add(obstacles.get(3));
    }

    private void fieldConfig5() {
        obstacles.add(new Circle(new Point(350,70),50));
        obstacles.add(new Rectangle(new Point(300,215), 120 , 100));
        obstacles.add(new Rectangle(new Point(300,410), 120, 100));
        obstacles.add(new Rectangle(new Point(515,125), 120 , 100));
        obstacles.add(new Circle(new Point(565,375),50));
        add(obstacles.get(0));
        add(obstacles.get(1));
        add(obstacles.get(2));
        add(obstacles.get(3));
        add(obstacles.get(4));
    }

    private void collisionDetectionTest() {
        Collections.sort(obstacles);
        Point free = new Point(170, 290);
        robot.changeLocation(free);
        System.out.println(robot.collideWith(obstacles.get(1)));
        System.out.println(obstacles.get(1).className);
        for (int i = 0; i < obstacles.size(); i++)
            System.out.println(robot.collideWith(obstacles.get(i)));
        System.out.println(collisionDetection(free));
    }

    private void sortingTest(){
        for (int i = 0; i < obstacles.size(); i++)
            System.out.println("Obstacle Left X :" + obstacles.get(i).top_left.x);
        Collections.sort(obstacles);
        System.out.println("After Sorting ---------");
        for (int i = 0; i < obstacles.size(); i++)
            System.out.println("Obstacle Left X :" + obstacles.get(i).top_left.x);
    }

    private void roadmapTest() {
        roadmap = new Roadmap(20, robot.center.x, robot.center.y);
        Point p = new Point(250, 250);
        int near = roadmap.nearestTo(p);
        roadmap.addVertexAndEdge(near, p);
        p = new Point(400,120);
        near = roadmap.nearestTo(p);
        roadmap.addVertexAndEdge(near, p);
        p = new Point(400,400);
        near = roadmap.nearestTo(p);
        roadmap.addVertexAndEdge(near, p);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
        if (show_tree.get())
            roadmap.showTree(g);
        if (show_path.get())
            roadmap.showPath(g);
    }

    protected Point ptOnLineAxisX(Point p1, Point p2, int x) {
        float dy = p2.y - p1.y;
        float dx = p2.x - p1.x;
        float k = dy / dx;
        int y = Math.round(k * (x - p1.x) + p1.y);
        return new Point(x, y);
    }

    public Point localPlanner(int nearest, Point x_rand) {
        Point ptNearest = roadmap.getCoordPoint(nearest);
        int distance = x_rand.x - ptNearest.x;
        int step = Math.round(distance / (num_steps + 1));
        Point res_point = ptNearest;
        for (int i = 1; i <= num_steps; i++) {
            Point new_point = ptOnLineAxisX(ptNearest, x_rand, ptNearest.x + (i * step));
            robot.setLocation(new_point);
            if (collisionDetection(new_point))
                return res_point;
            else
                res_point = new_point;
        }
        if(!collisionDetection(x_rand))
            res_point = x_rand;

        return res_point;
    }

    public boolean collisionDetection(Point new_robot_pos) {
        robot.changeLocation(new_robot_pos);
        for (int i = 0; i < obstacles.size(); i++) {
            if (robot.bottom_right.x > obstacles.get(i).top_left.x &&
                    robot.top_left.x < obstacles.get(i).bottom_right.x) {
                if (robot.collideWith(obstacles.get(i))) {
//                    System.out.println("Collide with " + i);
                    return true;
                }
            }
        }
        return false;
    }

    protected boolean withinField(Point new_center_pos) {
        Point new_top_left = new Point(new_center_pos.x - robot.radius , new_center_pos.y - robot.radius);
        Point new_bottom_right = new Point(new_center_pos.x + robot.radius, new_center_pos.y + robot.radius);

        if (new_top_left.x >= 0 && new_top_left.y >= 0 && new_bottom_right.y <= height && new_center_pos.x <= width)
            return true;
        return false;

/*        if (new_top_left.x >= 0 && new_top_left.y >= 0 &&
                new_bottom_right.x <= width && new_bottom_right.y <= height)
            return true;
        return false;*/
    }

    public Point randPoint() {
        Point randPt;
        do {
            int rand_x = (int) (Math.random() * (width + 1));
            int rand_y = (int) (Math.random() * (height + 1));
            randPt = new Point(rand_x, rand_y);
        } while (!withinField(randPt));
        return randPt;
    }

    public boolean isGoalAchieved() {
        Point tmp = roadmap.getCoordPoint(roadmap.size() - 1);
        if (tmp.x >= (width - 5) && tmp.x <= width)
            return true;
        return false;
    }


}

