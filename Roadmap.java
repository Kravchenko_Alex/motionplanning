package motionPlanning;

import com.sun.org.apache.regexp.internal.RE;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.util.ArrayList;

class Roadmap {

    /*   Array which hold parent node of the current node should be in class which make search (BFS, Dikstra)
     its OK here because we have Tree - special graph with no cycles
     that is why there is no function that create edge between two already existing vertexes =>
     that`s why there is no func that create only vertex (after that you cannot check is it existed before)*/

    private ArrayList<ArrayList<Integer>> adj; //the last node will be our goal
    private ArrayList<Point> coordinates;
    private ArrayList<Integer> parent;
    final int n_max;       // maximum nodes

    Roadmap(int max_nodes, int start_x, int start_y) {
        adj = new ArrayList<>();
        coordinates = new ArrayList<>();
        parent = new ArrayList<>();
        n_max = max_nodes;
        adj.add(new ArrayList<Integer>());
        coordinates.add(new Point(start_x, start_y));
        parent.add(0);
    }

    public Point getCoordPoint(int v) { return coordinates.get(v); }

    public int getParent(int v) { return parent.get(v); }

    public int addVertexAndEdge(int nearest, Point v) {
        validateVertex(nearest);
        if (n_max < adj.size()) {
            System.out.println("Too many nodes: max number = " + n_max);
            return -1;
        }
        adj.add(new ArrayList<Integer>());
        int v_index = adj.size() - 1;
        coordinates.add(v);
        parent.add(nearest);
        adj.get(nearest).add(v_index);
        adj.get(v_index).add(nearest);
        return v_index; // return new vertex index
    }

    private void validateVertex(int v) {
        if (v < 0 || v >= adj.size())
            throw new IllegalArgumentException("vertex " + v + " isn`t exist - you need to create it before");
    }

    //trace and draw path from start to last added vertex (goal)
    public void showPath(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        g2.setStroke(new BasicStroke(2));
        g2.setPaint(Color.green);

        int v = adj.size() - 1;
        do {
            int v_parent = getParent(v);
            g2.draw(new Line2D.Float(coordinates.get(v).x, coordinates.get(v).y,
                    coordinates.get(v_parent).x, coordinates.get(v_parent).y));
            v = v_parent;
        } while(parent.get(v) != 0);
        g2.draw(new Line2D.Float(coordinates.get(v).x, coordinates.get(v).y,
                coordinates.get(0).x, coordinates.get(0).y));

        g2.setPaint(Color.red);
        for (int i = 0; i < adj.size(); i++)
            g2.fill(new Ellipse2D.Float(coordinates.get(i).x - 4, coordinates.get(i).y - 4, 8, 8 ));
    }


    public void showTree(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;

        g2.setStroke(new BasicStroke(2));
        g2.setPaint(Color.black);
        for (int i = 0; i < adj.size(); i++) {
            for (int j = 0; j < adj.get(i).size(); j++) {
                int v_adj = adj.get(i).get(j);
                g2.draw(new Line2D.Float(coordinates.get(i).x, coordinates.get(i).y,
                        coordinates.get(v_adj).x, coordinates.get(v_adj).y));
            }
        }
        g2.setPaint(Color.red);
        for (int i = 0; i < adj.size(); i++)
            g2.fill(new Ellipse2D.Float(coordinates.get(i).x - 4, coordinates.get(i).y - 4, 8, 8 ));
    }

    public double distanceBetween(int v, int u) {
        double dx = coordinates.get(v).x - coordinates.get(u).x;
        double dy = coordinates.get(v).y - coordinates.get(u).y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    public double distanceBetween(Point v, int u) {
        double dx = v.x - coordinates.get(u).x;
        double dy = v.y - coordinates.get(u).y;
        return Math.sqrt(dx * dx + dy * dy);
    }

    //find nearest node in roadmap
    public int nearestTo(Point v) {
        if (v == null)
            throw new NullPointerException();

        int nearest = 0;
        double min_distance = Double.POSITIVE_INFINITY;
        for (int i = 0; i < adj.size(); i++) {
            double distance = distanceBetween(v, i);
            if(distance < min_distance) {
                min_distance = distance;
                nearest = i;
            }
        }
        return nearest;
    }

    public int size() {
        return coordinates.size();
    }

    public static void main(String[] args) {
        Roadmap r = new Roadmap(20,10,250);
        Point p = new Point(140, 80);
        int near = r.nearestTo(p);
        System.out.println(near);
        System.out.println(r.addVertexAndEdge(near, p));
        //System.out.println(r.getParent(1));
        p = new Point(80,120);
        near = r.nearestTo(p);
        System.out.println(near);
        System.out.println(r.addVertexAndEdge(near, p));
        p = new Point(30,300);
        near = r.nearestTo(p);
        System.out.println(near);
        System.out.println(r.addVertexAndEdge(near, p));
        p = new Point(170,50);
        near = r.nearestTo(p);
        System.out.println(near);
        System.out.println(r.addVertexAndEdge(near, p));
        p = new Point(100,140);
        near = r.nearestTo(p);
        System.out.println(near);
        System.out.println(r.addVertexAndEdge(near, p));
        System.out.println("---------------");
        System.out.println(r.getParent(1));
        System.out.println(r.getParent(2));
        System.out.println(r.getParent(3));
        System.out.println(r.getParent(4));
        System.out.println(r.getParent(5));

/*        Circle c1 = new Circle(new Point(4,4), 1);
        System.out.println(c1.getSize().height);
//        Circle c2 = new Circle(new Point(1,3), 2);
//        System.out.println(c1.collideWith(c2));
//        Rectangle r1 = new Rectangle(new Point(3,1), 2,2);
        Rectangle r2 = new Rectangle(new Point(5,3), 3,3);
        System.out.println(r2.collideWith(c1));
        System.out.println(c1.collideWith(r2));*/
    }

}
